from PIL import Image, ImageDraw, ImageFont
import csv
from math import floor, log10

SPEED = 32
ANGLE = "Sonata AD"
FILENAME = "sonata-ad_speed32.csv"

stage = "Outskirts"
# firt two numbers dimensions, second 2 offset from top left
stages = {
  "Outskirts": [1240, 510, 43, 33],

  "Stadium": [1230, 540, 48, 4],

  "Desert": [1130, 510, 98, 33],

  "Elevator": [1492, 522, 0, 92],

  "Factory": [1400, 542, 21, 58],

  "Streets": [1320, 515, 3, 29],

  "Sewer": [1240, 510, 43, 33],

  "Pool": [1210, 575, 58, 0],

  "Subway": [1050, 510, 138, 33],

  "Room": [1100, 550, 113, 20]
}

speed_disp = [
[1, 1, 1,   1, 0, 1,   1, 0, 1,   1, 0, 1,   1, 1, 1], #0
[0, 1, 0,   0, 1, 0,   0, 1, 0,   0, 1, 0,   0, 1, 0], #1
[1, 1, 1,   0, 0, 1,   1, 1, 1,   1, 0, 0,   1, 1, 1], # 2
[1, 1, 1,   0, 0, 1,   1, 1, 1,   0, 0, 1,   1, 1, 1], # 3
[1, 0, 1,   1, 0, 1,   1, 1, 1,   0, 0, 1,   0, 0, 1], # 4
[1, 1, 1,   1, 0, 0,   1, 1, 1,   0, 0, 1,   1, 1, 1], # 5
[1, 1, 1,   1, 0, 0,   1, 1, 1,   1, 0, 1,   1, 1, 1], # 6
[1, 1, 1,   0, 0, 1,   0, 0, 1,   0, 0, 1,   0, 0, 1], # 7
[1, 1, 1,   1, 0, 1,   1, 1, 1,   1, 0, 1,   1, 1, 1], # 8
[1, 1, 1,   1, 0, 1,   1, 1, 1,   0, 0, 1,   0, 0, 1] # 9
]
def game_to_pixel(x, y):
	return (int(stages[stage][0] / 2) + int(100*x) + stages[stage][2], stages[stage][1] - int(100*y) + stages[stage][3]) 

def digits(n):
    """generator which returns digits in left to right order. thanks stack overflow !"""
    k = floor(log10(n))
    for e in range(k,-1,-1):
        d,n = divmod(n,10**e)
        yield d

dig_width = 9
dig_marg = 2
num_offset = 7
bg_pad_x = 0
bg_pad_y = 10

def draw_display(draw, speed):
	d = list(digits(speed))
	center = stages[stage][0] / 2 + stages[stage][2]
	num_width = dig_width * 3 + dig_marg * 2 
	offset = (num_width) * len(d) / 2   + (num_offset * (len(d) -1 )/ 2)
		
	y_start = stages[stage][1] + stages[stage][3] + 70
	x_start = center - offset
	
	hw = dig_width + dig_marg
	fw = num_width + num_offset
	
	#sum_width = (len(d) - 1) * fw + hw * 3 + dig_width
	sum_width = 5 * fw + hw * 3 + dig_width
	sum_height = 4 * hw + dig_width
	draw.rectangle(
		(center - (sum_width / 2) - bg_pad_x, 
		y_start - bg_pad_y, 
		center + (sum_width / 2)  + bg_pad_x, 
		y_start + sum_height + bg_pad_y), 
		fill="#111111", outline="#ffffff",
					   width=2)	
	
	#debug center line
	#draw.rectangle((center - 5, 0, center + 5, 1000), fill="#ff0000") 
	num = 0
	for dig in d:
		disp = speed_disp[dig]
		
		for y in range(0, 5):
			for x in range(0, 3):
				if disp[y * 3 + x] == 1:
					draw.rectangle([
					(x_start + num * fw + hw * x, y_start + hw * y), 
					(x_start + num * fw + hw * x + dig_width, y_start + hw * y + dig_width)], 
					fill="#ffffff")
		num += 1

ball_jpg = Image.open('assets/ball.png')
def draw_ball(draw, pos):
	global ball_jpg
	#print(pos)
	draw.paste(ball_jpg, (pos[0] - 27, pos[1] - 26), mask=ball_jpg)
	
def draw_centered_text_and_shadow(draw, text, pos):
	w, h = draw.textsize(text, font=font)
	draw.text( (pos[0] + 1 - w//2, pos[1] + 1 - h//2), text, fill=(0, 0, 0), font=fontshadow)
	draw.text( (pos[0] - w//2, pos[1] - h//2), text, fill=(255, 255, 255), font=font)
img = Image.open('assets/Outskirts.jpg')

x = stages["Outskirts"][0]
y = stages["Outskirts"][1]
xOff = stages["Outskirts"][2]
yOff = stages["Outskirts"][3]
img1 = ImageDraw.Draw(img, 'RGBA')

font = ImageFont.truetype('GRATIS.ttf', 25);
fontshadow = ImageFont.truetype('GRATIS.ttf', 25);
fontdetails = ImageFont.truetype('GRATIS.ttf', 30);
# test bounds
#img1.rectangle([game_to_pixel(-6.2, 5.1), game_to_pixel(6.2, 0)], fill="#88888899")

with open(FILENAME, 'r') as file:
	reader = csv.reader(file)
	next(reader) # skip header
	i = 0
	pos = (-100000000000, -10000000000)
	lastpos = pos
	bounces = []
	for row in reader:
		pos = (float(row[0]), float(row[1]))
		img1.line([game_to_pixel(lastpos[0], lastpos[1]), game_to_pixel(pos[0], pos[1])], fill=(200, 200, 200, 100), width=14)
		if (i == 0):
			lastpos = pos
			draw_ball(img, game_to_pixel(pos[0], pos[1]))
		if pos[0] != lastpos[0] and pos[1] != lastpos[1]:
			draw_centered_text_and_shadow(img1, str(i), game_to_pixel(pos[0], pos[1]))
			
		else:
			bounces.append((game_to_pixel(pos[0], pos[1]), i))
			
		lastpos = pos
		i += 1
	for bounce in bounces:
		draw_ball(img, bounce[0])
		
	draw_ball(img, game_to_pixel(pos[0], pos[1]))
	

draw_display(img1, SPEED)
img1.text((20, img.size[1] - 50), ANGLE.upper(), font=fontdetails)
img.show()
