﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AngleSimulator
{
    public static class PluginInfo
    {
        public const string PLUGIN_GUID = "com.gitlab.axolotlll.frameanalysis";
        public const string PLUGIN_NAME = "AngleSimulator";
        public const string PLUGIN_VERSION = "1.0.0";
    }
}
