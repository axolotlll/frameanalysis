﻿using GameplayEntities;
using LLBML.Networking;
using StageBackground;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Configuration;
using System.Linq;
using System.Text;
using UnityEngine;

namespace AngleSimulator
{
    public class Simulator : MonoBehaviour
    { 
        private bool running;
        private static readonly float eps = 0.10f;

        private List<BallMovementData> data = new List<BallMovementData>();
        private string dataName = null;

        private bool endOnRight;

        void FixedUpdate()
        {
            if (running)
            {
                if (World.instance.ballHandler.GetBall(0) == null)
                {
                    data.Add(new BallMovementData(-100, -100, -100, -100));
                    return;
                }
                data.Add(new BallMovementData(GetBallX(), GetBallY(), GetBallVelX(), GetBallVelY()));

                if (!endOnRight && GetBallY() <= GetStageMinY() + eps)
                {
                    Stop();
                }
                else if (endOnRight && GetBallX() >= GetStageMaxX() - eps)
                {
                    Stop();
                }
            }
            
        }

        void OnGUI()
        {
            if (running)
            {
                GUILayout.BeginArea(AngleSimulatorPlugin.GuiPosition);
                GUILayout.BeginHorizontal(AngleSimulatorPlugin.HeaderAreaStyle);
                BallEntity ball = World.instance.ballHandler.GetBall(0);
                if (ball == null) return;
                float ballx = GetFloat(ball.entityData.position.GCPKPHMKLBN);
                float bally = GetFloat(ball.entityData.position.CGJJEHPPOAN);

                float ballvx = GetFloat(ball.ballData.velocity.GCPKPHMKLBN);
                float ballvy = GetFloat(ball.ballData.velocity.CGJJEHPPOAN);

                GUILayout.Label(String.Format("Pos({0:0.00},{1:0.00})", ballx, bally));
                GUILayout.Label(String.Format("Vel({0:0.00},{1:0.00})", ballvx, ballvy));

                GUILayout.EndHorizontal();
                GUILayout.EndArea();
            }

        }

        public void LoadRunScript(RunScript script)
        {
            foreach (RunCommand command in script.Commands)
            {
                if (command.type == RunCommands.NAME)
                {
                    SetName((string)command.field);
                }
                else if (command.type == RunCommands.ANGLE)
                {
                    SetAngle((float)command.field);
                }
                else if (command.type == RunCommands.SPEED)
                {
                    SetSpeed((int)command.field);
                }
                else if (command.type == RunCommands.POSITION)
                {
                    float[] vals = (float[])command.field;
                    SetPosition(vals[0], vals[1]);
                }
                else if (command.type == RunCommands.TEAM)
                {
                    int team = (int)command.field;
                    SetBallTeam(team);
                }
                else if (command.type == RunCommands.END)
                {
                    endOnRight = (bool)command.field;
                }
            }
        }

        public void Run()
        {
            if (running)
            {
                Stop();
            }

            data.Clear();
            Enable();

            // run setup
            //Test();

            running = true;
        }

        void Test()
        {
            SetName("testrun");
            SetSpeed(32);
            SetPosition(GetStageMinX(), GetStageMaxY());
            SetAngle(10f);
            SetBallTeam(0);
            endOnRight = false;
        }
        //
        public void Stop()
        {
            running = false;

            // get results
            AngleSimulatorPlugin.logger.LogInfo(dataName);
            if (dataName == null)
            {
                dataName = $"{DateTime.Now.ToUniversalTime().ToString("s").Replace(':', '-')}"; 
            }
            AngleSimulatorPlugin.SaveDataFile(dataName, DataToCSV());
            Disable();
        }
        //
        public bool IsRunning()
        {
            return running;
        }

        private void Enable()
        {
            //unpause game
            if (OGONAGCFDPK.instance.DFACBJGBGGA != null)
            {
                OGONAGCFDPK.instance.DFACBJGBGGA.exitProgress = 2;
            }

            // delete character
            World.instance.playerHandler.GetPlayerEntity(0).SetPlayerState(GameplayEntities.PlayerState.DISABLED);
        }

        private void Disable()
        {
            World.instance.playerHandler.GetPlayerEntity(0).SetPlayerState(GameplayEntities.PlayerState.NORMAL);
            World.instance.ballHandler.GetBall(0).SetBallState(GameplayEntities.BallState.STANDBY);
            SetPosition(0f, 0.9f);
            SetSpeed(8);

        }
        //TODO: make sure there is a ball in Slot 0, and add it if it's not there.
        private void ValidateBall()
        {
            // set the ball state
            World.instance.ballHandler.GetBall(0).SetBallState(GameplayEntities.BallState.ACTIVE);
        }
        void SetSpeed(int speed)
        {
            ValidateBall();
            World.instance.ballHandler.GetBall(0).ballData.flySpeed = IntToFloatf(speed);
        }

        void SetPosition(float x, float y)
        {
            ValidateBall();
            World.instance.ballHandler.GetBall(0).ballData.position.GCPKPHMKLBN = FloatToFloatf(x);
            World.instance.ballHandler.GetBall(0).ballData.position.CGJJEHPPOAN = FloatToFloatf(y);
        }

        void SetStage(string name)
        {
            // not implemented
        }

        void SetAngle(int angle)
        {
            // convert angle to floatf
            // Math.AngleToDirection()
            ValidateBall();
            World.instance.ballHandler.GetBall(0).ballData.flyDirection = Math.AngleToDirection(IntToFloatf(angle));
        }

        void SetAngle(float angle)
        {
            ValidateBall();
            World.instance.ballHandler.GetBall(0).ballData.flyDirection = Math.AngleToDirection(FloatToFloatf(angle));
        }

        void SetBallTeam(int slot)
        {
            ValidateBall();
            World.instance.ballHandler.GetBall(0).ballData.lastHitterIndex = slot;
        }
        
        void SetName(string name)
        {
            this.dataName = name;
        }


        static int GetAngle(Character chara, string name)
        {
            return 0;
        }

        static float GetStageMinX()
        {
            return GetFloat(World.instance.stageMin.GCPKPHMKLBN);
        }

        static float GetStageMinY()
        {
            return GetFloat(World.instance.stageMin.CGJJEHPPOAN);
        }

        static float GetStageMaxX()
        {
            return GetFloat(World.instance.stageMax.GCPKPHMKLBN);
        }

        static float GetStageMaxY()
        {
            return GetFloat(World.instance.stageMax.CGJJEHPPOAN);
        }

        static float GetBallX()
        {
            return GetFloat(World.instance.ballHandler.GetBall(0).ballData.position.GCPKPHMKLBN);
        }

        static float GetBallY()
        {
            return GetFloat(World.instance.ballHandler.GetBall(0).ballData.position.CGJJEHPPOAN);
        }

        static float GetBallVelX()
        {
            return GetFloat(World.instance.ballHandler.GetBall(0).ballData.velocity.GCPKPHMKLBN);
        }

        static float GetBallVelY()
        {
            return GetFloat(World.instance.ballHandler.GetBall(0).ballData.velocity.CGJJEHPPOAN);
        }

        static float GetFloat(HHBCPNCDNDH val) => (float)val.EPOACNMBMMN / 4.2949673E+09f;
        static HHBCPNCDNDH FloatToFloatf(float f) => HHBCPNCDNDH.GEIMAIJIPKI(f);
        static HHBCPNCDNDH IntToFloatf(int i) => HHBCPNCDNDH.NKKIFJJEPOL(i);

        string DataToCSV()
        {
            var sb = new StringBuilder("PosX,PosY,VelX,VelY");
            foreach(var point in data)
            {
                sb.Append('\n')
                    .Append(point.PosX)
                    .Append(",")
                    .Append(point.PosY)
                    .Append(",")
                    .Append(point.VelX)
                    .Append(",")
                    .Append(point.VelY);
            }

            return sb.ToString();
        }
    }
}
