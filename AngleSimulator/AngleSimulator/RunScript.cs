﻿using BepInEx.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AngleSimulator
{
    public class RunScript
    {
        public List<RunCommand> Commands = new List<RunCommand>();
        private ManualLogSource Logger => AngleSimulatorPlugin.logger;
        public RunScript() { }
        public RunScript(string scriptText)
        {
            Load(scriptText);
        }
        bool Load(string text)
        {

            string[] tokens = text.Split();
            for (int i = 0; i < tokens.Length - 1; i++)
            {
                if (tokens[i] == "NAME")
                {
                    // TODO: validate name
                    Commands.Add(new RunCommand(RunCommands.NAME, tokens[i + 1]));
                    i += 1;
                }
                else if (tokens[i] == "ANGLE")
                {
                    float angle = float.NaN;
                    if (float.TryParse(tokens[i + 1], out angle))
                    {
                        Commands.Add(new RunCommand(RunCommands.ANGLE, angle));
                    }
                    else
                    {
                        Logger.LogError("Could not read float from ANGLE.");
                        return false;
                    }
                    i += 1;
                }
                else if (tokens[i] == "SPEED")
                {
                    int speed = int.MinValue;
                    if (int.TryParse(tokens[i + 1], out speed))
                    {
                        Commands.Add(new RunCommand(RunCommands.SPEED, speed));
                    }
                    else
                    {
                        Logger.LogError("Could not read float from SPEED.");
                        return false;
                    }
                    i += 1;
                }
                else if (tokens[i] == "POSITION") // fix for strings min and max, or maybe dont just clamp
                {
                    // parse two values
                    float x = float.NaN;
                    float y = float.NaN;
                    if (!float.TryParse(tokens[i + 1], out x))
                    {
                        Logger.LogError("Could not read float value from X Position.");
                        return false;
                    }
                    if (!float.TryParse(tokens[i + 2], out y))
                    {
                        Logger.LogError("Could not read float value from Y Position.");
                        return false;
                    }

                    Commands.Add(new RunCommand(RunCommands.POSITION, new float[] { x, y }));
                    i += 2;

                }
                else if (tokens[i] == "TEAM")
                {
                    int team = int.MinValue;
                    if (int.TryParse(tokens[i + 1], out team))
                    {
                        Commands.Add(new RunCommand(RunCommands.TEAM, team));
                    }
                    else
                    {
                        Logger.LogError("Could not read float from TEAM.");
                        return false;
                    }
                    i += 1;
                }
                else if (tokens[i] == "END")
                {
                    if (tokens[i + 1] == "RIGHT")
                    {
                        Commands.Add(new RunCommand(RunCommands.END, true)); // TODO: document this, or make it better
                    }
                    else
                    {
                        Commands.Add(new RunCommand(RunCommands.END, false));
                    }
                    i += 1;
                }
                
            }
            return true;
        }

        public void DebugAllLines()
        {
            foreach (RunCommand command in Commands)
            {
                Logger.LogMessage(command.type.ToString() + "\t" + command.field.GetType().ToString() + "\t" + command.field.ToString());
            }
        }
    }

    public class RunCommand
    {
        public RunCommands type;
        public object field;

        public RunCommand(RunCommands type, object field)
        {
            this.type = type;
            this.field = field;
        }
    }
    public enum RunCommands
    {
        NAME = 0,
        ANGLE,
        SPEED,
        POSITION,
        TEAM,
        END
    }

   
}
