﻿using BepInEx;
using BepInEx.Logging;
using GameplayEntities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.Text;
using UnityEngine;
using UnityEngine.Experimental.UIElements;

namespace AngleSimulator
{
    [BepInDependency(LLBML.PluginInfos.PLUGIN_ID, BepInDependency.DependencyFlags.HardDependency)]
    [BepInDependency("no.mrgentle.plugins.llb.modmenu", BepInDependency.DependencyFlags.SoftDependency)]
    [BepInPlugin(PluginInfo.PLUGIN_GUID, PluginInfo.PLUGIN_NAME, PluginInfo.PLUGIN_VERSION)]
    public class AngleSimulatorPlugin : BaseUnityPlugin
    {

        public static bool IS_GAME_PAUSED => OGONAGCFDPK.instance.DFACBJGBGGA != null;
        public static bool IS_TRAINING => JOMBNFKIHIC.GIGAKBJGFDI.PNJOKAICMNN == GameMode.TRAINING;
        public static AngleSimulatorPlugin instance;
        public static ManualLogSource logger;
        private static RunScript currentRunFile;
        void Awake()
        {
            instance = this;
            logger = this.Logger;

            currentRunFile = GetRunFile();
        }

        public static void SaveDataFile(string name, string text)
        {
            string dest = Path.Combine(
                LLBML.Utils.ModdingFolder.GetModSubFolder(instance.Info).FullName,
                name + ".csv"
                );

            AngleSimulatorPlugin.logger.LogInfo(dest);

            File.WriteAllText(dest, text);
        }

        public static RunScript GetRunFile()
        {
            string dest = Path.Combine(
               LLBML.Utils.ModdingFolder.GetModSubFolder(instance.Info).FullName,
               "Runfile.txt"
               );

            if (File.Exists(dest))
            {
                RunScript runScript = new RunScript(File.ReadAllText(dest));
                runScript.DebugAllLines();
                return runScript;
            }

            
            logger.LogWarning("No Runfile.txt found");
            return null;

        }


        #region GUICODE
        public static GUIStyle ButtonStyle
        {
            get
            {
                GUIStyle gUIStyle = new GUIStyle(GUI.skin.label)
                {
                    fontSize = 15,
                    alignment = TextAnchor.MiddleCenter,
                    padding = new RectOffset(8, 8, 4, 4),
                    margin = new RectOffset(0, 0, 4, 4),
                    fixedHeight = 28,
                };

                gUIStyle.normal.textColor = Color.black;
                gUIStyle.normal.background = buttonBackground;
                gUIStyle.onNormal.background = buttonBackground;

                gUIStyle.hover.background = buttonHoverBackground;
                gUIStyle.onHover.background = buttonHoverBackground;

                gUIStyle.active.background = buttonActiveBackground;
                gUIStyle.onActive.background = buttonActiveBackground;
                return gUIStyle;
            }
        }
        static RectOffset rectOffset = new RectOffset(6, 6, 6, 6);
        public static GUIStyle AreaStyle
        {
            get
            {
                GUIStyle gUIStyle = new GUIStyle()
                {
                    padding = new RectOffset(6, 6, 6, 6),
                    margin = rectOffset,
                };
                gUIStyle.normal.background = areaBackground;
                return gUIStyle;
            }
        }

        public static GUIStyle HeaderAreaStyle
        {
            get
            {
                GUIStyle gUIStyle = new GUIStyle(AreaStyle)
                {
                    margin = new RectOffset(rectOffset.left, rectOffset.right, rectOffset.top, rectOffset.bottom),
                };
                return gUIStyle;
            }
        }

        private static Texture2D buttonBackground = ColorToTexture2D(Color.grey);
        private static Texture2D buttonHoverBackground = ColorToTexture2D(Color.green);
        private static Texture2D buttonActiveBackground = ColorToTexture2D(Color.white);
        private static Texture2D areaBackground = ColorToTexture2D(Color.black);

        static Texture2D ColorToTexture2D(Color32 color)
        {
            Texture2D texture2D = new Texture2D(1, 1, TextureFormat.RGBA32, false);
            texture2D.SetPixel(0, 0, color);
            texture2D.Apply();
            return texture2D;
        }

        public static Rect GuiPosition
        {
            get
            {
                return new Rect(Screen.width - GuiWidth, Screen.height - GuiHeight, GuiWidth, GuiHeight);
            }
        }

        static int GuiWidth = 400;
        static int GuiHeight = 50;

        bool reloadButton;
        bool startButton = false;
        bool simulating = false;
        float lastPress = 0f;

        

        void OnGUI()
        {
            
            if (IS_GAME_PAUSED && IS_TRAINING) {
                GUILayout.BeginArea(GuiPosition);
                GUILayout.BeginHorizontal(HeaderAreaStyle);

                reloadButton = GUILayout.Toggle(reloadButton, $"Reload", ButtonStyle);
                string startString = simulating ? "Stop"  : "Start";
                // start stop button
                startButton = GUILayout.Toggle(startButton, startString, ButtonStyle);

                if (reloadButton && Time.time - lastPress > 1.0f)
                {
                    
                    Logger.LogInfo("Reload pressed");
                    reloadButton = false;

                    currentRunFile = GetRunFile();

                    Simulator simulator = World.instance.gameObject.GetComponent<Simulator>();
                    if (simulator == null)
                    {
                        simulator = World.instance.gameObject.AddComponent<Simulator>();
                    }
                    simulator.LoadRunScript(currentRunFile);

                    lastPress = Time.time;
                }
                if (startButton && Time.time - lastPress > 1.0f)
                {

                    //handle start
                    Logger.LogInfo("Start pressed");
                    startButton = false;
                    
                    Simulator simulator = World.instance.gameObject.GetComponent<Simulator>();
                    if (simulator == null)
                    {
                        simulator = World.instance.gameObject.AddComponent<Simulator>();
                    }
                    simulator.LoadRunScript(currentRunFile);
                    simulator.Run();
                    

                    lastPress = Time.time;

                }

                GUILayout.EndHorizontal();
                GUILayout.EndArea();
            }
            
        }
        #endregion
    }
}
