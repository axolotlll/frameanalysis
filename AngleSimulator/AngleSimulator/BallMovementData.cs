﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AngleSimulator
{
    [Serializable]
    public class BallMovementData
    {
        public float PosX;
        public float PosY;
        public float VelX;
        public float VelY;

        public BallMovementData(float posX, float posY, float velX, float velY)
        {
            PosX = posX;
            PosY = posY;
            VelX = velX;
            VelY = velY;
        }

    }
}
